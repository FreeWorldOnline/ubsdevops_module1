import basic

#Read all the lines of the file into a list
with open("step_3.txt", "r") as file:
    lines = file.read().splitlines()

#Prepare values used during the processing
line_pos = 0
line_count = 0
seen_statements = []
grand_total = 0

#Iterate through the list to process each line
while line_pos < len(lines):
    line_count += 1
    line = lines[line_pos]
    parts = line.split(" ")

    #Check if we've seen this statement before
    if seen_statements.count(line) > 0:
        print(f'Stopping at line {str(line_pos + 1)} as it has been seen before: {line}')
        break
    seen_statements.append(line)

    #Hande straight forward gotos
    if (len(parts) == 2 and parts[0] == 'goto' and parts[1] != 'calc'):
        goto_line = 0
        try:
            goto_line = int(parts[1])
            if int(goto_line) > 0 and int(goto_line) <= len(lines):
                line_pos = int(goto_line) - 1
                print(f'Goto line {str(line_pos + 1)}')
            else:
                print(f'WARNING: Skipped goto at line {str(line_pos + 1)} as it was out of range ({str(goto_line)} < 1 or > {str(len(lines))}): "{line}"')
                line_pos += 1
        except ValueError:
            print(f'WARNING: Skipped line {str(line_pos + 1)} as it had an invalid goto line number: "{line}"')
            line_pos += 1
        continue

    #Handle goto with a calculation
    goto_result = False
    if (len(parts) == 5 and parts[0] == 'goto' and parts[1] == 'calc'):
        goto_result = True
        del parts[0]
    
    #Validate the line matches the expected format
    if (len(parts) != 4 or parts[0] != 'calc' or basic.allowed_ops.count(parts[1]) != 1):
        print(f'WARNING: Skipped line {str(line_pos + 1)} as it failed validation: "{line}"')
        continue

    #Parse the numbers into integers
    try:
        value_1 = int(parts[2])
        value_2 = int(parts[3])
    except ValueError:
        print(f'WARNING: Skipped line {str(line_pos + 1)} as it had an invalid numbver: "{line}"')
        continue

    #Now calculate the line and print the output
    result = basic.calculate(parts[1], value_1, value_2)
    grand_total += result
    print(f'{str(line_pos + 1)}) {line} = ' + str(result))

    #Goto the next line
    if goto_result:
        if int(result) > 0 and int(result) <= len(lines):
            line_pos = int(result) - 1
            print(f'Goto calulated line {str(line_pos + 1)}')
        else:
            print(f'WARNING: Skipped calulated goto at line {str(line_pos + 1)} as its result was out of range ({str(result)} < 1 or > {str(len(lines))}): "{line}"')
            line_pos += 1
            continue
    else:
        line_pos += 1

print()
print(f'Total lines processed: {str(line_count)}')
print(f'Total value: {str(grand_total)}')
print(f'Final line: {str(line_pos + 1)} - {lines[line_pos]}')