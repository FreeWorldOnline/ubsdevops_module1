To run manual calculations use the basic.py file and alter manual_mode to be True
To process the step_2.txt file with original rules run process_step2.py (after ensuring manual_mode in basic.py has been set back to False)
To process the step_3.txt file with original rules run process_step3.py
To process the step_4.txt file with original rules run process_step4.py