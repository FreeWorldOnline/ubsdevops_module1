import basic

#Read all the lines of the file into a list
with open("step_2.txt", "r") as file:
    lines = file.read().splitlines()

#Prepare value to store the grand total
grand_total = 0
line_count = 0

#Iterate through the list to process each line
for line in lines:
    line_count += 1
    parts = line.split(" ")

    #Validate the line matches the expected format
    if (len(parts) != 4 or parts[0] != 'calc' or basic.allowed_ops.count(parts[1]) != 1):
        print(f'WARNING: Skipped line {str(line_count)} as it failed validation: "{line}"')
        continue

    #Parse the numbers into integers
    try:
        value_1 = int(parts[2])
        value_2 = int(parts[3])
    except ValueError:
        print(f'WARNING: Skipped line {str(line_count)} as it had an invalid numbver: "{line}"')
        continue

    #Now calculate the line and print the output
    result = basic.calculate(parts[1], value_1, value_2)
    grand_total += result
    print(f'{str(line_count)}) {line} = ' + str(result))

print()
print(f'Total lines: {str(line_count)}')
print(f'Total value: {str(grand_total)}')