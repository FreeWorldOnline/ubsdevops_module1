#Manul mode?
manual_mode = False

#Main calculation function (used by)
allowed_ops = ('*', 'x', '+', '-', '/')
def calculate(oper, val1, val2):
    if (oper == '*' or oper == 'x'):
        return val1 * val2
    elif (oper == '+'):
        return val1 + val2
    elif (oper == '-'):
        return val1 - val2
    elif (oper == '/'):
        return val1 / val2

#Get the operation
if manual_mode:
    operation = input('Please enter the operation (' + ', '.join(allowed_ops) + '): ')
    while (allowed_ops.count(operation) == 0):
        print('Operation is not recognized!')
        operation = input('Please enter the operation (' + ', '.join(allowed_ops) + '): ')

    #Get the first value
    value_1 = input('Please enter the first value: ')
    while True:
        try:
            value_1 = int(value_1)
        except ValueError:
            print('Not a valid number!')
            value_1 = input('Please enter the first value: ')
        break

    #Get the second value
    value_2 = input('Please enter the second value: ')
    while True:
        try:
            value_2 = int(value_2)
        except ValueError:
            print('Not a valid number!')
            value_2 = input('Please enter the second value: ')
        break

    print('Result: ' + str(calculate(operation, value_1, value_2)))